//elementos que executam a ação da página

export const ContactElements = {
    idNome: 'input[id="contact-form-name"]',
    idEmail: 'input[id="contact-form-email"]',
    idAssunto: 'input[id="contact-form-subject"]',
    buttonEnviar: 'button[class="css-132u05m"]',
    idCompanhia: 'input[id="contact-form-company"]',
    idTelefone: 'input[id="contact-form-phone"]',
    classMsg: '[class="css-o45t51"]', 
    classConfirmacao: '[class="css-f1fp45"]'
}