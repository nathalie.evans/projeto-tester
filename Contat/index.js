//acoes de interação com a página
const elem = require('./elements').ContactElements; //require significa que está "solicitando" o elemento
const url = Cypress.config("baseUrl");

class Contact {
    //acessa o site
    acessarSite(){
        cy.visit(url);
    }
    //preenche o nome
    escreveNome(nome){
        cy.get(elem.idNome).click().type(nome); 
    }
    //preenche o email
    escreveEmail(email){
        cy.get(elem.idEmail).click().type(email);
    }
    //preenche o assunto
    escreveAssunto(assunto){
        cy.get(elem.idAssunto).click().type(assunto);
    }
    //preenche a mensagem
    preencheMsg(mensagem){
        cy.get(elem.classMsg).click().type(mensagem);
    }
    //preenche o nome da companhia
    escreveNomeCompanhia(empresa){
        cy.get(elem.idCompanhia).click().type(empresa);
    }
    //preenche o telefone
    escreveTelefone(telefone) {
        cy.get(elem.idTelefone).click().type(telefone);
    }
    //clica no botão enviar
    clicaBotaoEnviar(){
        cy.get(elem.buttonEnviar).click();
    }
    //validar mensagem de confirmação de envio
    confirmarEnvioMensagem(conclusao){
    const conclusoes = {"válido": "exist", "inválido": "not.exist"}
        cy.get(elem.classConfirmacao).should(conclusoes[conclusao]);
    }
    //
    validarMensagem(){
        cy.get('[class="css-f1fp45"]').should('exist'); 
    }
}

export default new Contact();