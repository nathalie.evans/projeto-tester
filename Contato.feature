#language: pt
Funcionalidade: Entrar em contato
   COMO criador de conteúdo do blog T
   DESEJO entrar em contato com os autores do blog FlexiBlog
   PARA fazer uma parceria. 

    #Regra: É possível usar o site em modo escuro
    Cenário: Mudar o site para o modo escuro
      Dado que visite o site
      Quando tento alterar para modo escuro 
      Então o site fica em modo escuro
    #Regra: É necessário preencher o Name para enviar a mensagem
    #Campo obrigatório
    Esquema do Cenário: Preencher o nome
      Dado que visite o site
      E não tenha nenhum dado em Name
      E o email "<email>", assunto "<assunto>" estejam preenchidos
      Quando preencho o Name com "<nome>"
      E tento enviar
      Então aparece uma mensagem de "<notificacao>"
      Exemplos:
        | nome    | email          | assunto        | notificacao |
        |         | test1@mail.com | Fazer Parceria | alerta      |
        | Marenna | test1@mail.com | Fazer Parceria | confirmação |
    #Regra: Company Name é opcional
    #Campo não obrigatório
    Esquema do Cenário: Preencher nome da companhia
      Dado que visite o site
      E não tenha nenhum dado em Company Name
      E o email "<email>", nome "<nome>" e assunto "<assunto>" estejam preenchidos
      Quando preencho o Company Name "<empresa>"
      E tento enviar
      Então o nome da companhia "<empresa>" é "<conclusao>"
      Exemplos:
        | nome    | email          | assunto        | empresa | conclusao |
        | Cameron | test2@mail.com | Fazer Parceria |         | válido    |
        | Cameron | test2@mail.com | Fazer Parceria | Solaryx | válido    | 
    #Regra: É necessário preencher o Email para enviar a mensagem
    #Campo obrigatório
    Esquema do Cenário: Preencher o email
      Dado que visite o site
      E não tenha dado em Email
      E o assunto "<assunto>" e nome "<nome>" estejam preenchidos
      Quando preencho o Email "<email>"
      E tento enviar
      Então é exibida uma mensagem de "<notificacao>"
      Exemplos:
        | nome    | email          | assunto        | notificacao |
        | Calores |                | Fazer Parceria | alerta      |
        | Calores | test3@mail.com | Fazer Parceria | confirmação |
    #Regra: Email precisa do caractere especial @ 
    #Campo obrigatório
    Esquema do Cenário: Preencher o email corretamente
      Dado que visite o site
      E o Email esteja vazio
      E o nome "<nome>" e assunto "<assunto>" estejam preenchidos
      Quando preencho o Email "<email>"
      E tento enviar
      Então é apresentada uma mensagem de "<alert>"
      Exemplos:
        | nome    | email          | assunto        | alert       |
        | Kirllon | test12.com     | Fazer Parceria | alerta      |
        | Kirllon | test4@mail.com | Fazer Parceria | confirmação |
    #Regra: Phone Number é opcional
    #Campo não obrigatório
    Esquema do Cenário: Preencher o número de telefone
      Dado que visite o site
      E nome "<nome>", email "<email>", assunto "<assunto>" estejam preenchido
      E não tenha dado em Phone Number
      Quando preencho com o número "<numero>"
      E tento enviar
      Então o número "<numero>" é "<conclusao>"
      Exemplos:
        | nome       | email          | assunto        | numero       | conclusao |
        | Evangeline | test5@mail.com | Fazer Parceria |              | válido    |
        | Evangeline | test5@mail.com | Fazer Parceria | 555 555-5555 | válido    |
    #Regra: Phone Number aceita letra e número
    #Campo não obrigatório
    Esquema do Cenário: Número de telefone aceita letras ou números
      Dado que visite o site
      E Phone number esteja vazio
      E o assunto "<assunto>", email "<email>" e nome "<nome>" estejam preenchido
      Quando preencho com o número de telefone "<numero>"
      E tento enviar 
      Então o telefone "<numero>" é "<conclusao>"
      Exemplos:
        | nome | email          | assunto        | numero       | conclusao |
        | Gisa | test6@mail.com | Fazer Parceria | 111 222-3333 | válido    |
        | Gisa | test6@mail.com | Fazer Parceria | abc def ghij | válido    |
    #Regra: Your Message é opcional
    #Campo não obrigatório
    Esquema do Cenário: Escrever sua mensagem
      Dado que visite o site
      E não tenha dado em Your Message
      E o nome "<nome>", assunto "<assunto>" e email "<email>" estejam preenchidos
      Quando preencho com a mensagem "<mensagem>"
      E tento enviar
      Então o texto "<mensagem>" é "<conclusao>"
      Exemplos:
        | nome  | email          | assunto        | mensagem                             | conclusao |
        | Shade | test7@mail.com | Fazer Parceria |                                      | válido    |
        | Shade | test7@mail.com | Fazer Parceria | Olá! Gostaria de fazer uma parceria. | válido    |
    #Regra: É necessário preencher o Subject para enviar a mensagem 
    #Campo obrigatório
    Esquema do Cenário: Preencher o assunto
      Dado que visite o site
      E o email "<email>", nome "<nome>" estejam completos
      E não tenha nenhum dado em Subject  
      Quando preencho com o assunto "<assunto>"
      E tento enviar
      Então uma "<notificacao>" é exibido
      Exemplos:
        | nome   | email         | assunto        | notificacao |
        | Farley | test@mail.com |                | alerta      |
        | Farley | test@mail.com | Fazer Parceria | confirmacao |
    #Regra: Limitar o número de caracteres do input Name
	  #Teste com 301 caracteres
    Esquema do Cenário: Verificar o limite de caracteres do Name
      Dado que visite o site
      E adiciono o nome "<nome>"
      E já tenha o email "<email>" e assunto "<assunto>" preenchidos
      Quando tento enviar 
      Então a mensagem é enviada
      Exemplos:
		    | nome                                                                                                                                                                                                                                                                                                          | email         | assunto    |
        | O vídeo fornece uma maneira poderosa de ajudá-lo a provar seu argumento. Ao clicar em Vídeo Online, você pode colar o código de inserção do vídeo que deseja adicionar. Você também pode digitar uma palavra-chave para pesquisar online o vídeo mais adequado ao seu documento. Por isso tem que ter limite. | tts1@mail.com | Divulgação |  
    #Regra: Limitar o número de caracteres do input assunto
	  #Teste com 335 caracteres
    Esquema do Cenário: Verificar o limite de caracteres de Subject
      Dado que visite o site
      E adiciono o assunto "<assunto>"
      E já tenha o nome "<nome>" e email "<email>" preenchidos
      Quando tento enviar  
      Então a mensagem é enviada
      Exemplos:
		    | assunto                                                                                                                                                                                                                                                                                                                                         | nome   | email          |
        | Para dar ao documento uma aparência profissional, o Word fornece designs de cabeçalho, rodapé, folha de rosto e caixa de texto que se complementam entre si. Por exemplo, você pode adicionar uma folha de rosto, um cabeçalho e uma barra lateral correspondentes. Clique em Inserir e escolha os elementos desejados nas diferentes galerias. | Lenera | tts1@mail.com  |
    #Regra: Limitar o número de caracteres do input email
	  #Teste com 248 caracteres
    Esquema do Cenário: Verificar o limite de caracteres de Email
      Dado que visite o site
      E adicione o email "<email>"
      E já tenha o nome "<nome>" e assunto "<assunto>" preenchidos
      Quando tento enviar 
      Então um alerta é exibido
      Exemplos:
		    | email                                                                                                                                                                                                                                                    | nome   | assunto  |
        | Temaseestilostambemajudamamanterseudocumentocoordenado.QuandovoceclicaemDesigneescolheumnovotema@asimagensgraficoseelementosgraficosSmartArtsaoalteradosparacorresponderaonovotemaQuandovoceaplicaestilosostitulossaoalteradosparacoincidircomonovotema  | Lenera | Novidade |
    #Regra: É possível visitar o FlexiBlog
	  Cenário: Visitar página inicial do FlexiBlog
      Dado que visite o site
      Quando clico em FlexiBlog
      Então visito a página inicial do blog   