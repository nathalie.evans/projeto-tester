import Contact from '../../support/Contat/index'; 

When ('o email {string}, nome {string} estejam completos', (email, nome) => {
    Contact.escreveEmail(email);
    Contact.escreveNome(nome);
})
When ('não tenha nenhum dado em Subject', () => {
    cy.get('[id="contact-form-subject"]').should('contain','');
})
When ('preencho com o assunto {string}', (assunto) => {
    if(assunto == ''){
        return;
    }
    Contact.escreveAssunto(assunto);
})
When ('tento enviar', () => {
 Contact.clicaBotaoEnviar();
})
Then('uma {string} é exibido', (notificacao) => {  
    if(notificacao == "confirmacao"){
        Contact.validarMensagem();
        cy.screenshot();
        return;
    }
    cy.get('[id="contact-form-subject"]').then(($input) => {
    expect($input[0].validationMessage).to.eq('Preencha este campo.');
    })
    cy.screenshot();
})