import Contact from '../../support/Contat/index'; 

When ('não tenha nenhum dado em Company Name', () => {
    cy.get('input[id="contact-form-company"]').should('contain',''); 
})
When ('o email {string}, nome {string} e assunto {string} estejam preenchidos', (email, nome, assunto) => {
    Contact.escreveEmail(email);
    Contact.escreveAssunto(assunto);
    Contact.escreveNome(nome);
})
When ('preencho o Company Name {string}', (empresa) => {
    if(empresa == ""){
        return;
    }
    Contact.escreveNomeCompanhia(empresa);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('o nome da companhia {string} é {string}', (empresa, conclusao) => {  
    cy.get('input[id="contact-form-company"]').should('exist', empresa); 
    Contact.confirmarEnvioMensagem(conclusao);
    cy.screenshot();
})