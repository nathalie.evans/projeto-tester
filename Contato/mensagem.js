import Contact from '../../support/Contat/index'; 

When ('não tenha dado em Your Message', () => {
    cy.get('[id="contact-form-message"]').should('contain', '') 
})
When ('o nome {string}, assunto {string} e email {string} estejam preenchidos', (nome, assunto, email) => {
    Contact.escreveNome(nome);
    Contact.escreveAssunto(assunto);
    Contact.escreveEmail(email);
})
When ('preencho com a mensagem {string}', (mensagem) => {
    if(mensagem == ""){
        return;
    }
    Contact.preencheMsg(mensagem);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('o texto {string} é {string}', (mensagem, conclusao) => {  
    cy.get('[id="contact-form-message"]').should('exist', mensagem) 
    Contact.confirmarEnvioMensagem(conclusao);
    cy.screenshot();
})