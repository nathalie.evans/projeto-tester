import Contact from '../../support/Contat/index'; 

When ('adicione o email {string}', (email) => {
    Contact.escreveEmail(email);
})
When ('já tenha o nome {string} e assunto {string} preenchidos', (nome, assunto) => {
    Contact.escreveAssunto(assunto);
    Contact.escreveNome(nome);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('um alerta é exibido', () => {   
    cy.get('[id="contact-form-email"]').then(($input) => {
      expect($input[0].validationMessage).to.eq('Insira um endereço de e-mail.');
    })
    cy.screenshot();
})