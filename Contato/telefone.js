import Contact from '../../support/Contat/index'; 
 
When ('nome {string}, email {string}, assunto {string} estejam preenchido', (nome, email, assunto) => {
    Contact.escreveNome(nome);
    Contact.escreveEmail(email);
    Contact.escreveAssunto(assunto);
})
When ('não tenha dado em Phone Number', () => {
    cy.get('input[id="contact-form-phone"]').should('contain', ''); 
})
When ('preencho com o número {string}', (numero) => {
    if(numero == ""){
        return;
    }
    Contact.escreveTelefone(numero);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('o número {string} é {string}', (numero, conclusao) => {  
    cy.get('input[id="contact-form-phone"]').should('exist', numero); 
    Contact.confirmarEnvioMensagem(conclusao);
    cy.screenshot();
})