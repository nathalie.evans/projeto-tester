When ('clico em FlexiBlog', () => {
    cy.xpath('.//*[@id="gatsby-focus-wrapper"]/div/div[1]/div/div/div[1]/a/div').click(); 
})
Then('visito a página inicial do blog', () => {   
    cy.url().should('be.equal','https://flexiblog-beauty.web.app/');
    cy.screenshot();
})