import Contact from '../../support/Contat/index'; 

When ('adiciono o nome {string}', (nome) => {
    Contact.escreveNome(nome);
})
When ('já tenha o email {string} e assunto {string} preenchidos', (email, assunto) => {
    Contact.escreveEmail(email);
    Contact.escreveAssunto(assunto);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('a mensagem é enviada', () => {   
    cy.get('[class="css-f1fp45"]').should('exist'); 
    cy.screenshot();
})