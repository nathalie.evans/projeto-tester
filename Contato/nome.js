import Contact from '../../support/Contat/index'; 

When ('não tenha nenhum dado em Name', () => {
    cy.get('input[id="contact-form-name"]').should('not.exist'); 
})
When ('o email {string}, assunto {string} estejam preenchidos', (email, assunto) => {
    Contact.escreveEmail(email);
    Contact.escreveAssunto(assunto);
})
When ('preencho o Name com {string}', (nome) => {
    if(nome == ""){
        return;
    }
    Contact.escreveNome(nome);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then ('aparece uma mensagem de {string}', (notificacao) => {   
    if(notificacao == "confirmação"){
        Contact.validarMensagem();
        cy.screenshot();
        return;
    }
    cy.get('[id="contact-form-name"]').then(($input) => {
        expect($input[0].validationMessage).to.eq('Preencha este campo.');
    })
    cy.screenshot();
})