import Contact from '../../support/Contat/index'; 

When ('adiciono o assunto {string}', (assunto) => {
    Contact.escreveAssunto(assunto);
})
When ('já tenha o nome {string} e email {string} preenchidos', (nome, email) => {
    Contact.escreveEmail(email);
    Contact.escreveNome(nome);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('a mensagem é enviada', () => {   
    Contact.validarMensagem();
    cy.screenshot();
})