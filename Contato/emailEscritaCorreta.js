import Contact from '../../support/Contat/index'; 

When ('o Email esteja vazio', () => {
    cy.get('input[id="contact-form-email"]').should('contain', ''); 
})
When ('o nome {string} e assunto {string} estejam preenchidos', (nome, assunto) => {
    Contact.escreveAssunto(assunto);
    Contact.escreveNome(nome);
})
When ('preencho o Email {string}', (email) => {
    Contact.escreveEmail(email);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('é apresentada uma mensagem de {string}', (alert) => { 
    if(alert == "confirmação"){
        Contact.validarMensagem();
        cy.screenshot();
        return;
    }
    //erro na validação do alert, validação que não existe confirmação da mensagem 
    cy.get('[class="css-f1fp45"]').should('not.exist');
    cy.screenshot();
})