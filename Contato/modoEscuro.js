import Contact from '../../support/Contat/index'

Given('que visite o site', () => {
    Contact.acessarSite();
})
When('tento alterar para modo escuro', () => {
    cy.get('[class="rc-switch css-1v0b4j3"]').trigger('mouseover').click(); 
})
Then('o site fica em modo escuro', () => {
    cy.get('[class="css-r8vurw"]').should('exist');
    cy.screenshot();
})