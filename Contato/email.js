import Contact from '../../support/Contat/index'; 

When ('não tenha dado em Email', () => {
    cy.get('input[id="contact-form-email"]').should('contain', ''); 
})
When ('o assunto {string} e nome {string} estejam preenchidos', (nome, assunto) => {
    Contact.escreveAssunto(assunto);
    Contact.escreveNome(nome);
})
When ('preencho o Email {string}', (email) => {
    if(email == ""){
        return;
    }
    Contact.escreveEmail(email);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('é exibida uma mensagem de {string}', (notificacao) => {  
    if(notificacao == "confirmação"){
        Contact.validarMensagem();
        cy.screenshot();
        return;
    }
    cy.get('[id="contact-form-email"]').then(($input) => {
        expect($input[0].validationMessage).to.eq('Preencha este campo.');
        cy.screenshot();
    })
})