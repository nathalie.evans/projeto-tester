import Contact from '../../support/Contat/index'

When ('Phone number esteja vazio', () => {
    cy.get('[id="contact-form-phone"]').should('not.exist'); 
})
When ('o assunto {string}, email {string} e nome {string} estejam preenchido', (assunto, email, nome) => {
    Contact.escreveAssunto(assunto);
    Contact.escreveEmail(email);
    Contact.escreveNome(nome);
})
When ('preencho com o número de telefone {string}', (numero) => {
    Contact.escreveTelefone(numero);
})
When ('tento enviar', () => {
    Contact.clicaBotaoEnviar();
})
Then('o telefone {string} é {string}', (numero, conclusao) => {  
    cy.get('input[id="contact-form-phone"]').should('exist', numero); 
    Contact.confirmarEnvioMensagem(conclusao);
    cy.screenshot();
})